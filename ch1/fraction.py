#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 21 20:01:08 2017

@author: piyush
"""


class Fraction:
    def __init__(self, numerator, denominator):
        if not (isinstance(numerator, int) and isinstance(denominator, int)):
            raise TypeError("Numerator and Denominator should be integer")
        if denominator == 0:
            raise ZeroDivisionError("Denominator can't be 0")
        commonDivisor = gcd(numerator, denominator)
        self.numerator = numerator // commonDivisor
        self. denominator = denominator // commonDivisor
        
    def __str__(self):
        return "Fraction " + str(self.numerator) + "/" + str(self.denominator)
                
    def getNum(self):
        return self.numerator
    
    def getDen(self):
        return self.denominator
    
    def __add__(self, other):
        if isinstance(other, float):
            other = Fraction(*toFraction(other))
        num = self.getNum() * other.getDen() + self.getDen() * other.getNum()
        den = self.getDen() * other.getDen()
        return Fraction(num, den)
    
    def __neg__(self):
        return Fraction(-self.getNum(), self.getDen())
    
    def __sub__(self, other):
        return Fraction.__add__(self, Fraction.__neg__(other))
    
    def __mul__(self, other):
        num = self.getNum() * other.getNum()
        den = self.getDen() * other.getDen()
        return Fraction(num, den)
    
    def __truediv__(self, other):
        return Fraction.__mul__(self, Fraction(other.getDen(), other.getNum()))
    
    def __radd__(self, other):
        return Fraction.__add__(self, other)
    
    def __iadd__(self, other):
        return Fraction.__add__(self, other)
    
    
def gcd(a, b):
    if b == 0:
        return a
    return gcd(b, a % b)

def toFraction(val):
    return val.as_integer_ratio()
    