# -*- coding: utf-8 -*-
"""
Created on Mon Jun 19 14:40:23 2017

@author: mishrap
"""

def find_min1(num_list):
    min_value = num_list[0]
    for i in num_list:
        is_minimum = True
        for j in num_list:
            if  i > j:
                is_minimum = False
        if is_minimum:
            min_value = i
    return min_value


def find_min2(num_list):
    min_value = num_list[0]
    for i in num_list[1:]:
        if i < min_value:
            min_value = i
    return min_value


list1 = [-100, 4, 2, 90, -5, 0]



print(find_min1(list1))                

print(find_min2(list1))     