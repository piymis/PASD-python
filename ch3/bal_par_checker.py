from stack_adt import Stack

def balanced_paren_checker(my_string):
    s = Stack()

    for c in my_string:
        if c == '(':
            s.push(c)
        elif s.isEmpty():
            return False
        elif c == ')':
            s.pop()

    if s.isEmpty():
        return True
    return False
