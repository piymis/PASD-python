#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 19 22:05:44 2017

@author: piyush
"""
from stack_adt import Stack

def divideBy2(val):
    if val == 0:
        return '0'
    s = Stack()
    while val > 0:
        s.push(str(val % 2))
        val //= 2
    binVal = ''
    while not s.isEmpty():
        binVal += s.pop()
    return binVal