from stack_adt import Stack

def general_paren_checker(mystr):
    s = Stack()
    paren_map = {
            ')' : '(',
            '}': '{',
            ']': '['
            }

    for c in mystr:
        if c in ['(', '[', '{']:
            s.push(c)
        elif s.isEmpty():
            return False
        else:
            if s.peek() == paren_map[c]:
                s.pop()
            else:
                return False
    if s.isEmpty():
        return True
    return False
