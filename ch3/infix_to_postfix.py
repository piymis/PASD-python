from stack_adt import Stack

def infix_to_postfix(infix_expr):
    opstack = Stack()
    postfix_expr_list = []
    infix_expr_list = infix_expr.split()

    prec  = {}
    prec['('] = 1
    prec['+'] = 2
    prec['-'] = 2
    prec['*'] = 3
    prec['/'] = 3
    prec['**'] = 4
    for token in infix_expr_list:
        if token == '(':
            opstack.push(token)
        elif token in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' or token in '0123456789':
            postfix_expr_list.append(token)
        elif token == ')':
            top_token = opstack.pop()
            while top_token != '(':
                postfix_expr_list.append(top_token)
                top_token = opstack.pop()
        else:
            while not opstack.isEmpty() and prec[token] < prec[opstack.peek()]:
                postfix_expr_list.append(opstack.pop())
            opstack.push(token)

    while not opstack.isEmpty():
        postfix_expr_list.append(opstack.pop())

    return ' '.join(postfix_expr_list)

assert infix_to_postfix("( A + B ) * ( C + D )") == 'A B + C D + *'
assert infix_to_postfix("( A + B ) * C") == 'A B + C *'
assert infix_to_postfix("A + B * C") == 'A B C * +'
