from queue_adt import Queue
def get_safe_position(num_people, start_point, num_skip, direction=1):
    people_queue = Queue()
    for i in range(1, num_people + 1):
        people_queue.enqueue(i)

    c = people_queue.dequeue()

    if start_point != 1:
        while c != start_point - 1:
            people_queue.enqueue(c)
            c = people_queue.dequeue()

    if direction == 1:
        while people_queue.size() > 1:
            for i in range(num_skip):
                people_queue.enqueue(people_queue.dequeue())
            people_queue.dequeue()

    return people_queue.dequeue()

print(get_safe_position(5, 1, 1, 1))
assert get_safe_position(5, 1, 1, 1) == 3
