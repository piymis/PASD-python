# -*- coding: utf-8 -*-
"""
Created on Tue Jun 20 15:26:45 2017

@author: mishrap
"""

class Queue:
    def __init__(self):
        self.items = []
        
    def enqueue(self, item):
        self.items.insert(0, item)
    
    def dequeue(self):
        return self.items.pop()
    
    def size(self):
        return len(self.items)
    
    def isEmpty(self):
        return self.items == []
    
    
    

