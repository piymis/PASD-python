#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 19 21:29:49 2017

@author: piyush
"""
from stack_adt import Stack

def revstring(mystr):
    s = Stack()
    for ch in mystr:
        s.push(ch)
    mystr = ''
    while not s.isEmpty():
        mystr += s.pop()
    return mystr
