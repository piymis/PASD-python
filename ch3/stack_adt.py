#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Stack ADT
Created on Mon Jun 19 21:15:39 2017
@author: piyush
"""


class Stack():
    def __init__(self):
        self.items = []
    
    def push(self, val):
        self.items.append(val)
        
    def pop(self):
        return self.items.pop()
        
    def peek(self):
        return self.items[len(self.items) - 1]
        
    def isEmpty(self):
        return self.items == []

    def size(self):
        return len(self.items)
    
    def __str__(self):
        return 'Stack: ' + str(self.items)
